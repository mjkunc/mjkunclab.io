---
title: My name is Mike
subtitle: I'm a data engineer.
comments: false
---

Thanks for stopping by. A few things you might want to know about me.

- Python is my favorite language
- Golang has been catching my interest lately 
- Testing is cool
- DevOps and automation is also cool

Check out my cv or my github or gitlab.

### my history

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
